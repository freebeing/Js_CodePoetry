const poetries = [
    //在正确的时间里遇见了你，从此便离不开了你
    `   function newTime(effort) {
       if (effort == true)
            return success
    }
`,
    //山无棱天地和乃敢与君绝
    `
    for(int i=0;i<earth.length;i++){
        println(i.loving(you));
    }
    throw new Exception("wrong object");
`,

    `   while (timeleft()>0)
        if(canmove()==true)
            printf ("Protect you" )
`,

    //
    `        long lovetime;
        if(love==true) for(lovetime=1;lovetime>=1;lovetime++)
`,

    `
    while(you.say('see u tomorrow')){
        l.sleeptime = 0
    }
`,
    //
    `
    int choice1 = Mother,choice2=You;
    if(Mother>River&&You>River){
        return choice2;
    }
`,

    `   if(you.hand.isCode() && weather.isWinter()){
        giveYouLove(my.hand.temperature,you.hand.temperature);
        return you.happyface;
    }
`,
    //每时每刻都在想你。
    `   void missingYou(){
        for(int time = meetYouTime();time++;){
            missingYou();
        }
   }
`

]

export default {
    data: {
        pointer: 0,
        poetry: poetries[2],
        footnote: "//",
        To: "To",
        Recipients: "Alice",
        From: "From ",
        DatePrefix: "Date  ",
        name: "Donut",
        date: "2022-02-14",
        direction: 1,
        //
        frame_w: 730,
        frame_h: 140,
        frame2_w: 820,
        frame2_h: 140,
    },
    onInit() {

    },
    onShow() {

    },
    onAttached() {
        setTimeout(() => {
            this.draw()
        }, 200)
        setTimeout(() => {
            this.loopPlay();
        }, 4200)
    },

    loopPlay() {
        this.pointer = Math.min(Math.max(this.pointer + this.direction, 0), poetries.length - 1);
        this.poetry = poetries[this.pointer]
        this.draw2();
        if (this.pointer == 0 || this.pointer == poetries.length - 1) {
            this.direction *= -1;
        }
        console.log("this.pointer:"+this.pointer)
        setTimeout(() => {
            this.loopPlay()
        }, 4000)
    },
    draw2(){
        this.showPart2()
        this.showPart3()
    },
    prePeotry() {
        this.pointer = Math.max(this.pointer - 1, 0);
        console.log('this.pointer:' + this.pointer)
        this.poetry = poetries[this.pointer]
        this.draw()
    },
    nextPoetry() {
        this.pointer = Math.min(Math.max(this.pointer + 1, 0), poetries.length - 1);
        console.log('this.pointer:' + this.pointer)
        this.poetry = poetries[this.pointer]
        this.draw()
    },

    draw() {
        this.drawFrame1()
        this.drawFrame2()
        this.showPart1()
        this.showPart2()
        this.showPart3()
        this.showPart4()
    },

    showPart1() {
        let options = {
            duration: 2000,
            delay: 0
        };
        let keyframes = [
            {
                transform: {
                    translate: '-400px 0px',
                },
            },
            {
                transform: {
                    translate: '0px 0px',
                },
            }
        ]
        let animation = this.$refs.part1.animate(keyframes, options);
        animation.play();
    },
    showPart4() {
        let options = {
            duration: 2000,
            delay: 10
        };
        let keyframes = [
            {
                transform: {
                    translate: '400px 0px',
                },
            },
            {
                transform: {
                    translate: '0px 0px',
                },
            }
        ]
        let animation = this.$refs.part4.animate(keyframes, options);
        animation.play();
    },

    showPart2() {
        let options = {
            delay: 2,
            duration: 1000,
        };
        let keyframes = [
            {
                transform: {
                    scale: 0.3,
                },
            },
            {
                transform: {
                    scale: 1,
                },
            }
        ]
        let animation = this.$refs.part2.animate(keyframes, options);
        animation.play();
    },
    showPart3() {
        let options = {
            delay: 2,
            duration: 1000,
            easing: 'ease-in',
        };
        let keyframes = [
            {
                transform: {
                    scale: 0.3,
                },
            },
            {
                transform: {
                    scale: 1,
                },
            }
        ]
        let animation = this.$refs.part3.animate(keyframes, options);
        animation.play();
    },

    drawFrame1() {
        const frame1 = this.$refs.frame1
        let ctx = frame1.getContext('2d')
        ctx.beginPath()
        ctx.moveTo(0, 0)
        ctx.lineTo(this.frame_w, 0)
        ctx.lineTo(this.frame_w * 0.9, this.frame_h * 0.5)
        ctx.lineTo(this.frame_w, this.frame_h)
        ctx.lineTo(0, this.frame_h)
        ctx.closePath()
        ctx.fillStyle = "#dc3e3f"
        ctx.fill();
    },
    drawFrame2() {
        const frame2 = this.$refs.frame2
        let ctx = frame2.getContext('2d')
        ctx = frame2.getContext('2d')
        ctx.beginPath()
        ctx.moveTo(0, 0)
        ctx.lineTo(this.frame2_w * 0.9, 0)
        ctx.lineTo(this.frame2_w, this.frame2_h)
        ctx.lineTo(0, this.frame2_h)
        ctx.closePath()
        ctx.fillStyle = "#dc3e3f"
        ctx.fill();
    }
}

const success = 1

//第一图，在正确的时间里遇见了你，从此便离不开了你
function newTime(effort) {
    if (effort == true)  return success
}

function missingYou() {

}

